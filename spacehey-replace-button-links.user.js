// ==UserScript==
// @name         Spacehey.com − Replace notifications buttons by links
// @namespace    https://misc.l3m.in/spacehey-notifications/
// @copyright    WTFPLv2
// @version      0.2
// @description  Now you can explore your notifications using ctrl+click and new tabs!
// @author       Corentin
// @match        https://*spacehey.com/*
// @icon         https://up.l3m.in/file/1693649328-spacehey.jpg
// @grant        none
// ==/UserScript==

const linkStyle = "background-color: #eee; border: 1px solid #aaa; display: inline-block; padding: 3px; border-radius: 3px; color: black;";

(() => {
    'use strict';
    Array.from(document.getElementsByClassName("open_notification_form")).forEach(function(notif) {
        var link = notif[1].value
        var a = document.createElement("a");
        a.href = link;
        a.innerHTML = notif[2].innerHTML;
        a.style = linkStyle;
        a.classList.add("linkThatReplacesAButton");
        notif.replaceWith(a);
    });

    let urls = Array.from(document.getElementsByClassName("linkThatReplacesAButton")).map(function (a) { return a.getAttribute("href") });
    let dedupedUrls = [ ...new Set(urls) ];

    let newTabButton = document.getElementById("newtab");
    let openAll = document.createElement("a");
    openAll.id = "openAll";
    openAll.innerHTML = "Open all notifications (" + dedupedUrls.length + " deduped urls)";
    openAll.title = "Open all your notifications in new tabs, one by one, each second. Deduplicate urls (url to /requests will appear only one time).";
    openAll.href = "#";
    openAll.style = "color: #aaf;";
    newTabButton.parentElement.appendChild(document.createElement("br"));
    newTabButton.parentElement.appendChild(openAll);

    document.getElementById('openAll').onclick = function() {

        dedupedUrls.forEach(function(url, index) {
            setTimeout( function () {
                let link = document.createElement('a');
                link.href = url;
                link.target = '_blank';
                link.click();
                console.log("[" + index + "] new link " + link.href + " clicked!");
            }, 1000*index);
        });
    };
})();